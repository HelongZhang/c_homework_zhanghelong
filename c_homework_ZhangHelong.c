#include <stdio.h>
#define MAX_LEN 255
int main()
{
	int lines = 0,mistakes = 0;
	char cBuf[MAX_LEN];		//存在中文文件中每次读取的行
	char eBuf[MAX_LEN];		//存在外文文件中每次读取的行
	memset(cBuf,0,MAX_LEN);
	memset(eBuf,0,MAX_LEN);
	FILE *fpc,*fpe,*out;
	fpc = fopen("Chinese.txt","r");
	fpe = fopen("English.txt","r");
	out = fopen("out.txt","w");
	if(NULL == fpc || NULL == fpe)
    {
        return -1; 
    }
	while(fgets(cBuf,MAX_LEN,fpc) != NULL && fgets(eBuf,MAX_LEN,fpe) != NULL)
	{
		char *cp = cBuf;
		char *ep = eBuf;
		bool isError = 0;
		lines++;
		while(1)		//处理每次从中文，英文txt中读出的字符串，查找'%',只有两种情况
		{
			if((cp = strchr(cp,'%')) != NULL)		//中文没到尾，英文也没到尾，中英文%后面相同就继续向后走，否则error
			{
				ep = strchr(ep,'%');
				if(NULL != ep && (*(++cp) == *(++ep)))
				{
					continue;
				}
				else{
					isError = 1;
					break;
				}
			}
			else								//只有中英文同时到达尾，此出口点才能保证此行没有错误，否则error
			{
				if(NULL == strchr(ep,'%'))
					break;
				else{
					isError = 1;
					break;
				}
			}
		}
		if(isError)
		{
			fprintf(out,"%d  %d行\n",++mistakes,lines);
			fprintf(out,"原始字符串:\n  %s",cBuf);
			fprintf(out,"出错字符串:\n  %s",eBuf);
		}
		memset(cBuf,0,MAX_LEN);
		memset(eBuf,0,MAX_LEN);
	}
	fclose(fpc);
	fclose(fpe);
	fclose(out);
	return 0;
}

